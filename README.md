# Environments

Started out as an exploration of using GitLab environments to place a link directly to browsable artifacts in an MR (possible) and ended up exploring ways to pull artifacts with CURL from within a later job using the job token (harder if possible at all).

Note the job token attempts (all now commented out) rely on the project being private. I made it public after exploration for sharing.
